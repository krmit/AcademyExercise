# Grindar I

Låt en användare skriva in två värden. Om det första värdet är 1 så sätter vi variabel A till “true” annars sätter vi variabel till “false” och sedan på samma sätt för en variabel B. Gör en if-sats som när villkoret är sant skriver ut 1 och om det är falskt skriver ut 0. Gör ett villkor som klarar av alla tester i denna uppgift.

by: krm
