//by olback
/*
 *	Get the median of any amount of numbers
 * 	This is a modified version https://github.com/olback/code-snippets/tree/master/js/median
 * 	olback Â© 2018
 */

let arr = [];

/* 
 *	Every time prompt() is called, it reads a new line. Read until a negative number is found.
 *	If the number is greater than -1, push it to the array.
 */
while (input = prompt()) {
    if (Number(input) > -1)
        arr.push(input);
    else
        break;
}

/*	
 *	0. Javascript array method-chaning is beatuful!
 *	1. Convert everything in the array to numbers.
 *	2. Filter out the values that are NaN by comparing it with itself, because in Javascript, NaN !== NaN.
 * 	3. Finaly sort the array, starting with the smallest number.
 */
arr = arr.map(x => Number(x)).filter(val => val === val).sort((a, b) => a - b);

/* 
 *  If there are an even amount of numbers, print the avrige of the two in the middle,
 *	else print the median.
 */
if (arr.length % 2 === 0) {
    console.log((arr[(arr.length / 2) - 1] + arr[(arr.length / 2)]) / 2);
} else {
    console.log(arr[Math.floor(arr.length / 2)]);
}
