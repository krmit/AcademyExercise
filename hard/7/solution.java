package N7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class N7 {
		   static File infil;
		   static Scanner sc;
		   
		   public static void main(String[] args) {

		      // läser  in alla rader
		      open("data.txt");
		      
             int count=0;
		     // läser in en rad från filen.
		      while(more()){
		         //Läser in rad från filen.
		         String line = read();
		         count+=line.length();
		         
		      }
		      
		      System.out.println(count);
		      sc.close();
		      
		   }
		   
		   public static void open(String fileName){
		      try {
		         infil = new File(fileName);
		         sc= new Scanner(infil);
		      } catch (FileNotFoundException ex) {
		         System.out.println("Filen hitades inte");
		      }
		   }
		   public static boolean more(){
		      return sc.hasNext();
		   }
		   public static String read(){
		      String line="";
		      line= sc.nextLine();
		      return line;   
		   }
}
