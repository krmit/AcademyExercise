# Räkna alla bokstäver i en fil.

Du ska läsa in en fil på hårdisken genom att läsa rad för rad. Du också passa på att räkan alla bokstäver. 

För java:
Du hittar filen du ska läsa här: http://htsit.se/p/hard/7/data.txt
Du kan utgå från följande kod: http://htsit.se/p/hard/7/code.java

För js:
Du kan anvädna denna kod: http://htsit.se/p/hard/7/code.js
Den kod läser egentligen inge fil från hårdisken, så det är egentligen lite fusk. Därför måste du använda de funktioner som beskrivs i koden.


**Tips:** Du kan använda dig av Math.random() som har ett värde mellan 0 och 1.

by: krm, goran
