# Goto lista

Låt en användaren skriva in så många heltal denna vill och avsluta med något som inte är ett heltal. Vi ska nu tolka denna lista på följande sätt:
Uygå ifrån första talet i listan. Gå till positionen i listan som dett ta motsvarar. Till exempel är första talet en 3:a så gå till posistion 3 i tabellen och skriver ut det talet. Vi utgåt sedan i från talet på denna position. Om ett tal är större än listan så avbryt tolkningen. Tolkningen ska också avbrytas om antal uppslag som görs är fler en längden på listan. Se testerna för exempel.  

category: array
by: krm
