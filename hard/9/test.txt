not (P and Q) = not P or not Q

in:
1
1

out:
1

in:
1
0

out:
1

in:
0
1

out:
1

in:
0
0

out:
0
