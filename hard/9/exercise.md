# De Morgans lag I

Låt en användare skriva in två värden. Om det första värdet är 1 så sätter vi variabel A till “true” annars sätter vi variabel till “false”. På samma sätt för en variabel B. Gör nu ett if sats med ett villkor som ska var A eller B. Men i villkoret får du inte  inte använda operatorn “||”, bara operatorn “&&” och “!”.  I if-satsen skriv ut “1” om sant och i else-delen skriv ut “0”.

**Tips:** Sök på internet efter “De Morgans lag”

by: krm
