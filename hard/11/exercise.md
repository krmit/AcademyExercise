# De Morgans lag II

Låt en användare skriva in två värden. Om det första värdet är 1 så sätter vi variabel A till “true” annars sätter vi variabel till “false”. På samma sätt för en variabel B. Gör nu en if-sats med ett villkor så att alla tester blir sanan.  I if-satsen skriv ut “1” om sant och i else-delen skriv ut “0”.
Men i villkoret får du inte  inte använda operatorn “&&”, bara operatorn “||” och “!”.

by: krm
