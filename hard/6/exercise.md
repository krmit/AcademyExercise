# En enkel gauss kurva ifrån addition

Att adera saker gör att vi kommer få mindre slump! Pröva att slumpa ut tre decimal tal mellan 0 och 1 och addera dem. Dividera summan med 3. Multiplicera sedan med 10 och avrunda sedan nedåt. Du har nu ett tal mellan 0 och 9. Gör detta många gånger och räkna hur många du får av varje siffra. Skriv ut ditt resultat på ett snyggt sätt.

**Tips:** Du kan använda dig av Math.random() som har ett värde mellan 0 och 1.

by: krm
