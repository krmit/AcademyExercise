# Kryptera och dekryptera på enkelt sätt

Definiera två funktioner en kalla crypt och en decrypt. Båda funktionerna tar varsitt heltal. Crypt adderar, subtraherar, multiplicerar eller dividerar något med sin parameter, gör en sådan operation minst tre gånger. Resultatet av en sådan beräkning ska sedan returneras. Gör sedan så att funktionen decrypt tar ett tal som returneras från crypt och returnerar den parameter som crypt fick för att returnera talet. Till exempel ska det som står nedan bli sant:

decrypt(crypt(4)) === 4

I testerna nedan utgår vi ifrån att det tal som användaren skriver in ska först ges som parameter till crypt och resultatet ska ges till decrypt.


by: krm
