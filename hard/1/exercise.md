# Fibonaccital med rekursion

Fibonaccital är en mycket vanlig förekommand uppgift på diverse programmeringsprov och tävlingar. Ofta maskerade på ett eller annat sätt. Det finns olika sätt att beräkna dem men här ska vi använda oss av rekursion, dvs att vår funktion ska anropa sig själv. 

Vi ska ha en funktion, som vi kallar f, som tar en heltalsparameter, som vi här kallar x och som dessutom måste vara större än eller lika med 0. Om x är lika med 0 ska funktionen returnera 0 och om x är lika med 1 ska funktionen returnera 1. I annat fall ska funktionen returnera f(x-1)+f(x-2), dvs den ska anropa sig själv igen men minska parametrarna. Gör ett program som beräknar det n:te fibonaccitalet där n är ett heltal som användaren ger.

by: krm
category: recursion
next: VH1
