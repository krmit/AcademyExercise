# Caesarchiffer

Gör ett program som tar ett ord och ett heltal som indata. Gör sedan ett Caesarchiffer, http://sv.wikipedia.org/wiki/Caesarchiffer, som krypterar ordet genom att låta varje bokstav “hoppa” fram lika många steg som det angivna heltalet. Du behöver inte bry dig om vad som händer om du hoppar utanför alfapetet.


**Tips:** Du kan göra om en string till en lista av bokstäver genom koden nedan. Det kommer göra att det är lättare att manipulera texten.
**java:** *word_char= word.toCharArray();*

by: krm, memeQueen
