# Beräkna rötterna till en andragradsekvation!

Gör en funktion som löser andragradsekvationer och skriver ut lösningen i konsolen. Utgå ifrån pq-formlen:

`x = -p/2-+sqrt((p/2)2 - q)`

för `x2 + px + q = 0`

Tänk på att du måste kontrollera om `(p/2)^2 - q` är större än 0. Om det inte är det så ska du skriva ut “Finns inga reella rötter”.

Kontrollera att detta fungerar genom att skriva ett program som tar emot värden på p och q för att sedan returnera rötterna. Den största rotten ska skrivas ut först. Undvik att skriva ut onödiga nollor eller för många decimaler.   

by: krm, Crash, hamstern 
