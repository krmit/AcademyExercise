# Att beräkna Pi

π är ett mycket berömd tal. Den har oändligt med decimaler som aldrig återupprepas. Att försöka hitta så många decimaler på π har alltid varit en rolig lek för programmerare Här tittar vi på en rolig metod som använder sig av slump.

Man kan uppskatta värdet på π genom att införa två variabler x och y. Man slumpar ett värde mellan 0,0 och 1,0 för både x och y. Om x*x+y*y < 1 så hamnar punkten inom en kvartscirkel. I annat fall hamnar den utanför.
 
Gör ett program som gör en simulering med ett antal iterationer som man läser in och som beräknar hur många av punkterna som uppfyller villkoret ovan. Dela detta tal med totalt antal iterationer och multiplicera med 4. Skriv ut resultatet! Om antalet iterationer är stort så skall du hamna nära 3,1415.
 
**Tips:** Du kan använda dig av Math.random() som har ett värde mellan 0 och 1.

by: krm
