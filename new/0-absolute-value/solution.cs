//by: Maximus

   static void Main(String[] args)
   {
      Console.WriteLine("Skriv in ett tal");
      int a = int.Parse(Console.ReadLine().ToString());
      Console.WriteLine(Convert(a).ToString());
   }
 
   static int Convert(int i)
   {
      if (i < 0)
         i = -i;
 
      return i;
   }
