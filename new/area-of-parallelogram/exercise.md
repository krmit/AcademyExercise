---
contribute: krm
copyright: Magnus Kronnäs
title: Arean av parallellogram!
level: veryEasy
tags: function
---

Gör en funktion som tar två inparametrar height och base. Utifrån dessa beräkna arean av ett parallellogram som har dessa egenskaper.
