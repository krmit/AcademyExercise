# Att säga rätt “Hej”

På olika språk säger man hej på olika sätt. Se den enkla tabell:

Språk                   Förkortning     Hälsning
Franska                 fr              Bonjour
Svenska                 se              Hej
Tyska                   ge              Hallo
Italienska              it              Ciao
Vietnamesiska           vi              Xin chào

Gör ett program där användaren skriver in en förkortning enligt tabellen ovan och programmet skriver ut hälsningsfrasen. Du ska bara använda if-satser.

by: krm
