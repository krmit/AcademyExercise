# Skriv in och ut några konfigurationer

Låt en användare skriva in två värden. Det första värdet ska vara en nyckel och det andra ett värde. Tillsammans utgör de en egenskap. Spara denna egenskap i ett objet så att object[nyckel]=värde. 
Skriv sedan ut objektet. Titta gärna i test för att se hur utskriften ska vara.

by: krm
