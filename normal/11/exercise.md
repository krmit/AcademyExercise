# Skap en lista med multiplar.

Låt användarnen skriva in en tal. Skapa en lista med tio element som på första elementet har talet, på andra element har talet multiplicerat med två och så vidare. Skriv sedan ut listan.

by: krm
category: array
