# Tecken summa

Ibland kan det vara bra om vi kan jämför om två texter är samma utan att ha de två texter vi vill jämföra. Till exempel för att se om någon skrivit in rätt lösenord. Då kan vi göra om det sparade lösenordet och det lösenord vi får från en användare till ett tal och se om detta tal är samma.

Gör ett program som frågar efter ett ord med små bokstäver från “a” till “z”. Du ska låta varje bokstav ha ett värde. “a” ska vara värt 1, “b” ska vara värt 2 och så vidare fram till “z” som är värt 26 . 
 
Programmet ska sedan gå igenom alla bokstäver i ordet och summera dem utifrån deras värden. Skriv ut detta värde.

Tips: Du kan använda metoden charCodeAt.  Till exempel ger "a".charCodeAt(0) värdet 97.

by: krm
