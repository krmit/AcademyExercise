# Beräkna ett polynom

Gör ett program som beräknar värdet av polynommet _3x^3+2x^2+x+0_. En användare ska skriva in ett heltalsvärdet på x och programmet ska skriva ut svaret på polynommet.

**Tips:** Du kan för att få fram vad x^3 är skriva koden "x*x*x".

by: krm, imareza
