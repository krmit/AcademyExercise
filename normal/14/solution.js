//by imareza

//Ta emot input
var x = prompt("Enter a Value");

//Gör talet till int datatyp
var num = parseInt(x);

//Nollställ resultat
var result = 0;

//En loop för att räkna ut polynomet.
for(var i=3; i>0; i--){  
  var grad = i*Math.pow(num, i);
  
  result = grad + result;  
}

//Skriv ut 
console.log(result); 
