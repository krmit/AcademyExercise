//by gardNGnome

import java.util.Random;
import java.util.Scanner;
/*
 * Created by: "GardNGnome" 2017-05-06
 */

public class N10 {

	private static Scanner input;

	public static void main(String[] args) {
		/* Defines variables and opens for user inputs. 
		*/
		Random rand = new Random();
		int numberToGuess = rand.nextInt(100);
		int numberOfTries = 0;		
		input = new Scanner(System.in);
		int guess;
		boolean win = false;
		
		/*
		 * Creates a loop that continues as long as win=false.
		 */
		while (win == false){
			
			System.out.println("Guess a number between 1 and 100: ");
			guess = input.nextInt();
			numberOfTries++;
			
			/*
			 * If guess = numberToGuess a win message is shown. 
			 * The message show number of tries and what the correct number was.
			 */
			if (guess == numberToGuess){
				win = true;
				System.out.println("You win!");
				System.out.println("The number was " + numberToGuess);
				System.out.println("It took you " + numberOfTries + " tries!");
			
			}
			/*
			 * If numberToGuess is < or > than "guess" a message will tell you if 
			 * your "guess" was higher or lower than numberToGuess.
			 */
			else if (guess < numberToGuess){
				System.out.println("Your guess is too low, try again! ");
			}
			else if (guess > numberToGuess){
				System.out.println("Your guess is to high, try again! ");
			}

				
		}

	}

}
