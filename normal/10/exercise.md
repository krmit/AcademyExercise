# Gissa ett tal

Gör ett program som ska fungera som spelet “Gissa tal”. Datorn ska slumpa ut talet mellan 0 och 100. Användaren får nu gissa ett tal. Om användaren gissade fel säger datorn om talet är större eller lägre och låter användaren gissa igen. I annat fall föklarar datorn att användaren har vunnit och avslutar spelet.

**Tips:** En "random" funktion finns i de flesta språk. De ger ett decimaltal mellan 0 och 1. Multiplicera med 101 och använd sedan en inbyggd funktion som "floor" eller konvertera till ett heltal. Du får då ett tal mellan 0 och 100.

**Obs:** Test är inte möjliga på denna uppgift. 

by: krm
