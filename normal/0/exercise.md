# Skapa en talföljd av jämna tal

Vi utgår ifrån ett tal. Om talet är udda så forsätter du inte med talföljen. I annat fall så dividerar du med två och forsätter med det talet. Gör ett program som låter användaren ge ett start värde och sedan skriver ut talföljden.

tips: Du behöver använda en loop som loopar medan talet är jämt och sedan slutar. För att se om ett tal är jämt använd följande kod för att som om variabel j är jämn:
js: j % 2 == 0

by: krm, hamstern
