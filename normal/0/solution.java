//by hamstern
package normal;

import java.util.Scanner;

/**
 * Created by Hamstern on 2017-04-19.
 */
public class Ex0
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        // Create a object to that'll receive inputs
        // System.out.println("Number?");
        // Print a text
        int number = in.nextInt();
        for(;;)
        {
            System.out.println(number);
            // Print the number
            if(number % 2 == 0)
            {
                number = number/2;
            }else{
                break;
            }
            //look if you can divide number with 2 else break the loop to finish the process
        }
        // A loop that'll stop when the number below isn't dividable
    }
}
