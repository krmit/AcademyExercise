# Beräkna kostnad för en beställning.

Du ska räkna ut kostnaden för att köpa motstånd i en butik för elektriska komponeter. Varje motstånd kostar 50 öre, dvs 0,5 kronor. Köparen måste köpa minst 100 st. Om köparen försöker köpa färre än 100 stycken ska programmet skriva ut "Medges ej" Om köparen köper mer än 1000 motstånd får denna 1% rabbat. Om köparen köper mer än 10000 motstånd får denna 10% rabbat. Ditt program ska fråga köparen efter hur många motstånd och skriva ut kostnaden i kronnor.

by: krm
