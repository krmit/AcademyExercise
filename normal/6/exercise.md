# Jämn eller udda bokstav

Gör ett program som tar ett ord som in data. Kontrollera sedan om den första bokstaven har ett jämt eller udda ascii nummer. Om numret är jämnt skriv ut "even", annars "odd".

**Exempel:** “a” har ascii värdet 97, så det är det vi här menar med en udda bokstav.

**Tips:** Koden nedan kommer göra så att första bokstaven i text variabeln *word* kommer att lagras som ett heltal i variabeln *ascii-code*.  
**java:** *int ascii-code = (int)word.charAt(0);* 
**js:** *let ascii-code = word.charCodeAt(0);* 

by: krm
