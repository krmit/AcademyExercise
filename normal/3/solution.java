//by manssonlife
import java.util.Scanner;

public class N3 {

        public static void main(String[] args) {
                      Scanner in = new Scanner(System.in);

                      int number  =  in.nextInt();

                      if (number <= 100){
                                 number += 25;
                      }

                      if (number <= 500){
                                 number += 50;
                      }

                      if (number > 1000){
                                 number *= 0.975;
                      }
                      System.out.println(number);
           }
}
