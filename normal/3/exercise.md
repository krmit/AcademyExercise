# Beräkna kostnad med frakt och rabat.

Du ska räkna ut kostnaden i en e-butik för den som handlar. Har du handlat för mer än 100 kr så behöver du inte betala fakturaavgiften på 25 kronnor. Om du handlar för mer än 500 kr får du fri frakt som annars kostar 50 kr. Handlar du för mer än 1000 kr får du 2,5% rabat på allt.

Gör ett program som frågar efter hur mycket kunden har handlat för och sedan skriver ut hur mycket kunden ska betala.

by: krm
