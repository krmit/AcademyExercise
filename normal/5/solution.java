//by sweecrow
import java.util.Scanner;

public class Ex5 {

	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String ord; 
		char bokstav; 
		int ascii_code; 
 
		System.out.println("Skriv en Bokstav?");
		
		ord = sc.next();
		bokstav = ord.charAt(0);
		ascii_code = (int)bokstav-97;
		
		System.out.println(ascii_code);
	}

}
