# Boksatav till tal

Låt oss numrera våra tal så att “a” är 0 och “b” är osv. Gör ett program som tar en bokstav och returnerar talet för denna bokstav. Du behöver bara göra för talen a-z.

**Tips:** Koden nedan kommer göra så att första bokstaven i text variabeln *word* kommer att lagras som ett heltal i variabeln *ascii-code*.  
**java:** *int ascii-code = (int)word.charAt(0);* 
**js:** *let ascii-code = word.charCodeAt(0);* 

by: krm
