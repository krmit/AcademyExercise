# Medelvärde av tärningar

Fråga en användare hur många tärningar med sex sidor som ska slås. Slå sedan så många tärningar och beräkna medelvärdet av de tärningar du har slagit. Skriv ut medelvärdet.

**obs:** Vi kommer inte kunna göra ett test, men desto fler tärningar som slås desto oftare borde medevärdet vara i närheten av tre.

by: krm
