# Medelvärde med avslut

Detta bygger på en lek för barn: https://en.wikipedia.org/wiki/Fizz_buzz

Du ska skriva ut 100 tal. Ett nytt tal för varje rad. Förutom när:
    * Om ett tal är delbar med 3 ska du skriva Fizz.
    * Om ett tal är delbar med 5 ska du skriva Buzz.
    * Om ett tal är delbart med 3 och 5 skriv Buzz Fizz

by: krm
