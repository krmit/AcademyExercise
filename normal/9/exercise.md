# Beräkna absolutbelopet av en 2D vektor

Gör en funktion som beräknar absolutbelopet av en vektor som du låter representeras av två decimaltal. Anpassa sedan ditt program så det accepterar indata ifrån testfallen.

**Tips:** I detta fall kommer du bara behöva beräkna pythagoras sats på de två värderna, dvs 'sqrt(x^2+y^2)'. 

by: krm
