# Vilket tal är delbart?

Vi ska göra ett program som läser in ett heltal mindre än 100. Sedan får användaren forsätta skriva in tal tills användaren skriver in ett tal som är delbart med det första. Det vill säga att att det inte blir en reste när talet divideras med det tal som användare skrivit in. Till exempel 15 är delbar med 5 eftersom 15/5=3, men 16 är inte delbart med 5 eftersom 16/5=3.2.

by: krm
