# Låt användaren skriva in en lista.

Låt en användarnen skriva in en lista av posetiva heltal tal. Varje tal ska skrivas in för sig, när användaren skriver in ett negativ tal så inläsningen avbrytas. När användaren är klar ska du skriva ut alla talen igen. Du kan utgå ifrån att användaren inte skriver in mer än 100 tal. 

category: array
by: krm
