# Arean av en snögubbe

Tänk dig att du ritar en snögubbe som består av tre olika snöbollar. På ditt papper kommer snögubben bestå av tre olika cirklar. Vi antar här att de inte överlappar. Vad kommer då arean vara för snögubbarna?

Du ska använda en funktion för att beräkna arean av en cirekel. Dessutom ska du använda en funktion för att beräkna arean av hela snögubben, denna funktion måste ta tre inparametrar. Varje parameter står för radien av en cirkel. Låt sedan en användare ange dessa tre radier som indata till programmet.

by: krm
