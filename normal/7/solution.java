//by sweecrow
import java.util.Scanner;

public class Ex7 {

	
	
public static void main(String[] args) {
		
		        //scanner that asks for user input.
		
		        Scanner sc = new Scanner(System.in);
		
		        double radie1 = sc.nextDouble();
		        double radie2 = sc.nextDouble();
		        double radie3 = sc.nextDouble();
		        
		        double svar = area(radie1,radie2,radie3);
		
		        System.out.println(svar);
		    }

				//function to calculate total area.
				private static double area(double radie1, double radie2, double radie3) {
				
					return cirkel(radie1)+cirkel(radie2)+cirkel(radie3);
				}
				
				//function to calculate one circle area.
				private static double cirkel(double r) {
				
					return r*r*Math.PI;
				}	

}
