# Summan av tal!

Gör en funktion som tar två heltal som parameter. Låt oss kalla de första talet tal1 och det andra tal2. Du börjar med att du skriver ut tal1 på ny rad, sedan skriver du ut tal1+tal1 på nästa rad, sedan tal1+tal1+tal1. Forsätt med detta tal2 gånger. Se test som exemple.

by: krm
