# Från Radianer till Grader.

Gör ett program som övesätter från grader till radianer. Låt användaren skriva in i grader och få svaret i radianer.

**Tips:** Radianer = Grader/360*2*Pi
**Obs:** Testen kan visa fel pga avrundningar.

by: krm
