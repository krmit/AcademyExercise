# Öppet Intervall
Vi har ett öppet intervall [1,10]. Det vill säga att alla tal som är mellan 1 och 10, förutom 1 och 10.

Låt en användare skriva in ett tal och skriv ut 1 om taler är i intervallet och 0 om talet inte är i intervallet.


by: krm
