# Vad är mod?

Låt användaren ange två positiva heltal som inte är noll. Skriv sedan ut heltals kvoten och resten. För utskriften följ exemplet nedan som är för om en användaren skriver in 9 och 7:

9/7=1+2/7

**Tips:** Använd “%” och “Math.floor”.

by: krm
