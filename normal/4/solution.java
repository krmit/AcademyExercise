//by schlusqer
package normal;

import java.util.Scanner;

public class Ex0 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int sidaB = in.nextInt();
		int sidaC = in.nextInt();
		double vinkel = in.nextDouble();
		double resultat = sidaA(sidaB, sidaC, vinkel);
		System.out.println(resultat);
		in.close();
	}
		
	private static double sidaA(double sidaB, double sidaC, double vinkel) {
		return Math.sqrt(Math.pow(sidaB,2) + Math.pow(sidaC,2) - (2 * sidaB * sidaC * Math.cos(vinkel)));
	}
}
