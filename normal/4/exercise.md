# Cosinussatsen

Gör en funktion som använder sig av cosinusatsen för att beräkna en okänd vinkel. Gör ett program som låt användaren skriva in längder på två sidor och sedan avsluta med en vinkel. Längden ska vara i cm, medan vinkel ska vara i radianer. Du kan använda **double** som datatyp. Resultatet ska sedan vara den sida som är mitt emot vinkeln. Gör sedan ett program som läser in värden och skriver ut svaret.

**Tips:** Formeln är a² = b²+c² -2bc * cos(alpha) taget ifrån http://sv.wikipedia.org/wiki/Cosinussatsen.

**Obs:** Ibland kan det hända att ditt program får “svenska” inställnmingar, så då ska man skriva 3,14 istället för 3.14. 

by: krm
