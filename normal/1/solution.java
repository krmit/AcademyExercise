//by peronet

public class N1 {
	public static void main(String[] args) {
		//Gör ett program som skriver ut alla tal ifrån 1 till 100. 
		//Talen ska stå efter varandra på en rad, men max tio olika 
		//tal per rad och sedan en ny rad. Det ska vara ett mellanslag 
		//mellan varje tal. Använd minst en for-loop.
		
			
		for(int i=1;i<101;i++){
            if(i%10==0)
                {System.out.print(i + "\n");}
		    else
                {System.out.print(i + " ");}
		}
	}
}
