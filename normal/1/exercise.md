# Skriv ut 100 tal på ett snyggt sätt.

Gör ett program som skriver ut alla tal ifrån 1 till 100. Talen ska stå efter varandra på en rad, men max tio olika tal per rad och sedan en ny rad. Det ska vara ett mellanslag mellan varje tal. Använd minst en for-loop.

extension of: VE1
by: krm
