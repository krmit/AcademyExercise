# Dubelt så många "dubbelt"!

Gör en funktion som tar ett heltal som parameter och skriver ut “dubbelt” dubbelt så många gånger som heltalet anger. Varje “dubbelt” ska vara på en ny rad. Du ska testa det med ett program där en användare anger hur många "dubbelt" som ska skrivas ut.

variant: VE5
by: krm, Crash 
