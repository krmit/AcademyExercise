# Säg BOOM

En teorist har jämt en bomb på alla tal som är delbara med 5. Skriv ut alla tal ifrån 1 till 100. Du ska skriva varje tal på en ny rad. Men om talet är delbar med 5 ska du skriva "BOOM".

tips: Om ett tal är delbar med 5 så kommer det ha resten noll det delas med 5. Till exempel 5, 10, 15 ,20 osv. Låt i vara ett heltal och följande utryck kommer vara sant om i är delbart med 5.
js: i % 5 == 0

by: krm
