var editor;
var promt_list=[] ;
var log_index=0;
var dataset=[{"in":"","out":"","test":"","status":null}];
var dataset_index = 0;
var solutions={};
var number_of_buttons = 1;

var exercise_seletion;
var exercise_level; 
    
var exercise_number;
    
var exercise_base_url;
var exercise_url;

var solution_lang;

// Load in an new 

function load() {
    resetButtons();
    
    exercise_seletion = document.getElementById("level");
    exercise_level = exercise_seletion.options[exercise_seletion.selectedIndex].value;
    
    exercise_number = document.getElementById("number").value;
    
    exercise_base_url = "./"+exercise_level+"/"+exercise_number;
    exercise_url = exercise_base_url+"/exercise.md?"+config["version"];

    client = new HttpClient();
    client.get(exercise_url, function(response) {
    var exercise_data = parseExercise(response);
    document.getElementById("exercise-title").innerHTML = exercise_data["title"];
    document.getElementById("exercise").innerHTML = exercise_data["text"];
    
    if(exercise_data.hasOwnProperty("by")) {
    document.getElementById("by").innerHTML = exercise_data["by"];
    }
    
    if(exercise_data.hasOwnProperty("category")) {
    document.getElementById("category").innerHTML = exercise_data["category"];
    }
});

// Loading test
var test_url = exercise_base_url+"/test.txt?"+config["version"];

client.get(test_url, function(response) {
var test_data = parseTest(response);
dataset=test_data["data"];
    
while(dataset.length > number_of_buttons) {
        addDataset();
}

selectDataset(0);
});

    // Loading solution
    var config_url = exercise_base_url+"/config.json?"+config["version"];
    
    document.getElementById('solution-info').style.display = 'none';

    client.get(config_url, function(response) {
		console.warn("hej");
		console.warn(response);
	
    solutions = JSON.parse(response); 
    if(solutions["solution"]===0) {
        document.getElementById('solutions').style.display = 'none';
}
    else {
        document.getElementById('solutions').style.display = 'initial';
        checkSolutionFor("java");
        checkSolutionFor("md");
        checkSolutionFor("cs");
        checkSolutionFor("js");
        document.getElementById(solution_lang).selected = true;
        document.getElementById("solution-lang").onchange();
}
});
};

function checkSolutionFor(lang) {
    if(solutions[lang]===0) {
        document.getElementById('solution-'+lang).style.display = 'none';
}
    else {
        document.getElementById('solution-'+lang).style.display = 'initial';
        solution_lang='solution-'+lang;
}
}

function loadSolution() {
    
    var solution_seletion = document.getElementById("solution-lang");
    var solution_lang = solution_seletion.options[solution_seletion.selectedIndex].value;
    var solution_number = document.getElementById("solution-by").value;
    var postfix=(solution_number>0)?("-"+solution_number):"";
    var solution_url = exercise_base_url+"/solution"+postfix+"."+solution_lang+"?"+config["version"];
    
    client = new HttpClient();
    client.get(solution_url, function(response) {
        
    var solution_data = parseSolution(response);
    editor.getDoc().setValue(solution_data["code"]);
    
    if(solution_data.hasOwnProperty("by")) {
    document.getElementById('solution-info').style.display = 'initial';
    document.getElementById("solution-by-alias").innerHTML = solution_data["by"];
    }
});
}

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() { 
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.overrideMimeType("text/plain");            
        anHttpRequest.send( null );
    }
}

function parseExercise(text) {
    var parse = text.trim().split("\n")
    var result={};
    result["title"]=parse[0].slice(1);

    while(parse.slice(-1)[0].includes(":")) {
        var meta_text = parse.pop();
        var meta_list = meta_text.split(":"); 
        result[meta_list[0]]=meta_list[1];
    }
    result["text"]=parse.slice(1).join("<br />");

    return result;
}

function parseSolution(text) {
    var parse = text.trim().split("\n")
    var result = {};
    result["by"]=parse[0].split(" ").slice(1);
    result["code"]=parse.slice(1).join("\n");

    return result;
}

function parseTest(text) {
    var parse = text.split("\n")
    var parse_next=[];
    var parse_list=[];
    var result={};
    result["data"]=[];
    data_in=[];
    data_out=[];
    data_in_flag=false;
    data_out_flag=false;
    data_set={};

    for(var i = 0; i < parse.length; i++) {
        if(parse[i]==="in:" || parse[i]==="out:") {
            parse_next.push(parse_list);
            parse_list=[];
            parse_next.push(parse[i]);
        }
        else {
            parse_list.push(parse[i]);
        }
    }
    
    parse_next.push(parse_list);
    
    for(var i = 1; i < parse_next.length; i+=2) {
        if(parse_next[i]==="in:") {
           data_set["in"]= parse_next[i+1].join("\n").trim();
           if(i+2 < parse_next.length && parse_next[i+2]==="out:") {
            data_set["test"]= parse_next[i+3].join("\n").trim();;
            i+=2;
           }
        }
        else if(parse_next[i]==="out:") {
           data_set["test"]= parse_next[i+1].join("\n").trim();
        }
        data_set["out"]="";
        data_set["status"]=null;
        result["data"].push(data_set);
        data_set={};
    }
    
    return result;
}

// Resets buttons
function resetButtons() {
    dataset_index = 0;
    number_of_buttons = 1;
    var div = document.getElementById("selection");
    div.innerHTML = ""; //Remove old buttons.

    var button_plus = document.createElement("button");
    button_plus.onclick= function (){addDataset()};
    button_plus.id="add-Dataset";
    button_plus.appendChild(document.createTextNode("+"));
    div.appendChild(button_plus);
    
    var button_one = document.createElement("button");
    button_one.onclick= function (){changeDataset(0)};
    button_one.id="selection-0";
    button_one.appendChild(document.createTextNode(0));
    div.appendChild(button_one);
    selectButton(0);
}

function succeedButton(Index) {
    document.getElementById("selection-"+Index).classList.add("succeed");
    document.getElementById("selection-"+Index).classList.remove("fail");
}

function failButton(Index) {
    document.getElementById("selection-"+Index).classList.remove("succeed");
    document.getElementById("selection-"+Index).classList.add("fail");
}

function selectButton(Index) {
    document.getElementById("selection-"+Index).classList.add ("selected");
}

function flipButton(Index) {
    document.getElementById("selection-"+Index).classList.toggle ("selected");
}



// Run code

function runJS() {
    updateDataset();
    runIndexJS(dataset_index);
    selectDataset(dataset_index);
}

function updateDataset() {
    dataset[dataset_index]["in"]=document.getElementById("in").value;
    dataset[dataset_index]["test"]=document.getElementById("test").value;
}

function runIndexJS(Index) {
    dataset[Index]["out"] = "";
    log_index=Index;
    parseInput(Index);
    try {
    eval(editor.getValue());
    } catch (e) {
        console.log("Line: " +e.lineNumber +" Column: "+e.columnNumber +"\n"+e);
    }

    testOutput(Index);
}

function parseInput(Index) {
    var input_text = dataset[Index]["in"];
    promt_list = input_text.split("\n");
}

function testOutput(Index) {
    var out_text = dataset[Index]["out"].trim();
    var test_text = dataset[Index]["test"].trim();
    if(out_text===test_text) {
        dataset[Index]["status"]=true;
        succeedButton(Index);
    } else {
        dataset[Index]["status"]=false;
        failButton(Index);
    }
}



// Test all code

function testAll() {
    for(var i =0; i < dataset.length; i++) {
    runIndexJS(i);
}
}

// Save code
function codeChange(instance, changeObj) {
     code=instance.doc.getValue();
     localStorage.setItem("my-0", code);
 }

// Select a dataset for testing

function changeDataset(Selection) {
    updateDataset();
    selectDataset(Selection);
}

function selectDataset(Selection) {
    flipButton(dataset_index);
    flipButton(Selection);
    dataset_index=Selection;
    document.getElementById("in").value    =dataset[dataset_index]["in"];
    document.getElementById("out").value   =dataset[dataset_index]["out"];
    document.getElementById("test").value  =dataset[dataset_index]["test"];
}

// Add a new dataset
function addDataset() {
    number_of_buttons++;
    if(dataset.length < number_of_buttons) {
       dataset.push({"in":"","out":"","test":"","status":null}); 
    }
    
    var div = document.getElementById("selection");
    var button = document.createElement("button");
    var button_click_func = function(number) {return function() {changeDataset(number-1)}};
    button.onclick= button_click_func(number_of_buttons);
    button.id="selection-"+(number_of_buttons-1);
    button.appendChild(document.createTextNode(number_of_buttons-1));
    
    div.appendChild(button);
}

// Get a solution
function givenSolution(Solution) {
}

// Return to my solution
function mySolution() {
}

// Create editor
function createEditor() {
    
    prompt = function(message) {
                return promt_list.shift();
        }

    console.log = function(message) {
                dataset[log_index]["out"] += message+"\n";
        }
    
	  editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
          lineNumbers: true,
          mode:  "javascript",
});
editor.setSize(800, 200);
  editor.on('change', codeChange);
    if(localStorage.getItem("my-0")) {
document.getElementById("editor").value = localStorage.getItem("my-0");
}

createOptionForLevel(document.getElementById("level"));

selectDataset(0);
selectButton(0);
}

function createOptionForLevel(selection) {
    var level = selection.value;
    var max = config[level];
    
    number_select = document.getElementById('number');
    number_select.options.length = 0;

    for (var i = 0; i<max; i++){
       var opt = document.createElement('option');
       opt.value = i;
       opt.innerHTML = i;
       number_select.appendChild(opt);
    }
}

function createOptionForSolution(selection) {
    var lang = selection.value;
    var max = solutions[lang];

    //document.getElementById('solutions').style.display = 'initial';
    
    number_select = document.getElementById('solution-by');
    number_select.options.length = 0;

    for (var i = 0; i<max; i++){
       var opt = document.createElement('option');
       opt.value = i;
       opt.innerHTML = i;
       number_select.appendChild(opt);
    }
}

