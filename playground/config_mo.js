var config = 
{
    "veryEasy": {{veryEasy}},
    "easy": {{easy}},
    "normal": {{normal}},
    "hard": {{hard}},
    "veryHard": {{veryHard}},
    "extreme": {{extreme}},
    "exercises":{{number_of_exercises}},
    "solutions":{{number_of_solutions}},
    "solutions_uniq":{{number_of_solutions_uniq}},
    "version": {{version}}
};
