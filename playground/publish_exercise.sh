#!/bin/sh
TC='\033['
Rst="${TC}0m"     

Bold="${TC}1m"    
Undr="${TC}4m"    

Black="${TC}30m"; 
Red="${TC}31m"; 
Green="${TC}32m"; 
Yellow="${TC}33m";
Blue="${TC}34m";  
Purple="${TC}35m";
Cyan="${TC}36m";
White="${TC}37m";

export cs=`ls $1 | grep solution | grep .cs | wc -l`;
export md=`ls $1 | grep solution | grep .md | wc -l`;
export java=`ls $1 | grep solution | grep .java | wc -l`;
export js=`ls $1 | grep solution | grep .js | wc -l`;
export solution=`ls $1 | grep solution | wc -l`;
printf "$Bold Solution:$Rst $solution $Blue cs: $cs md: $md js: $js java: $java $Rst\n";

echo "{\"java\":$java, \"cs\":$cs, \"js\":$js, \"md\":$md, \"solution\":$solution}"> $1/config.json
