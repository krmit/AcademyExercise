#!/bin/sh
set -e;

TC='\033['
Rst="${TC}0m"     

Bold="${TC}1m"    
Undr="${TC}4m"    

Black="${TC}30m"; 
Red="${TC}31m"; 
Green="${TC}32m"; 
Yellow="${TC}33m";
Blue="${TC}34m";  
Purple="${TC}35m";
Cyan="${TC}36m";
White="${TC}37m";

export levels="veryEasy/* easy/* normal/* hard/* veryHard/* extreme/*"

export veryEasy=`ls veryEasy/ | wc -w`;
export easy=`ls easy/ | wc -w`;
export normal=`ls normal/ | wc -w`;
export hard=`ls hard/ | wc -w`;
export veryHard=`ls veryHard/ | wc -w`;
export extreme=`ls extreme/ | wc -w`;

export number_of_solutions_uniq=`find $levels -type f | grep solution | cut -d"/" -f 3 | uniq | wc -l`;
export number_of_solutions=`find $levels -type f | grep solution |  wc -l`;
export number_of_exercises=`find $levels -type d |  wc -l`;

export version=`cat version`;
echo "$version+1" | bc > version

printf "\n$Bold$Cyan Version: $version $Rst\n\n";

echo "var config = 
{
    'veryEasy': $veryEasy,
    'easy': $easy,
    'normal': $normal,
    'hard': $hard,
    'veryHard': $veryHard,
    'extreme': $extreme,
    'exercises': $number_of_exercises,
    'solutions': $number_of_solutions,
    'solutions_uniq': $number_of_solutions_uniq,
    'version': $version
};" > playground/config.js

find ./easy/* -type d | xargs -I % ./playground/publish_exercise.sh %
find ./veryEasy/* -type d | xargs -I % ./playground/publish_exercise.sh %
find ./normal/* -type d | xargs -I % ./playground/publish_exercise.sh %
find ./hard/* -type d | xargs -I % ./playground/publish_exercise.sh %
find ./veryHard/* -type d | xargs -I % ./playground/publish_exercise.sh %
find ./extreme/* -type d | xargs -I % ./playground/publish_exercise.sh %

echo "
<!doctype html>
<html lang='sv'>
<head>
<meta charset='utf-8' />
<title>HTSIT playground</title>
<script src='node_modules/codemirror/lib/codemirror.js'></script>
<link rel='stylesheet' href='node_modules/codemirror/lib/codemirror.css'>
<script src='node_modules/codemirror/mode/javascript/javascript.js'></script>
<script src='./playground/js/exercise-editor.js?$version'></script>
<script src='./playground/config.js?$version'></script>
<link rel='stylesheet' href='./playground/css/simple.css?$version'>
<script>
window.onload = createEditor
</script>
<body>
<h1 id='welcome' >Välkommen!</h1>
<div id='main'>

<div id='options'>
<select id='level' onchange='createOptionForLevel(this)'>
<option value='my'>my</option>
<option value='veryEasy'>very easy</option>
<option value='easy'>easy</option>
<option value='normal'>normal</option>
<option value='hard'>hard</option>
<option value='veryHard'>very hard</option>
<option value='extreme'>extreme</option>
</select>
<select id='number'></select>
<button id='load' onclick='load()'>Load</button>
</div>
<div id='solutions' style='display:none'> 
<p>Det finns följande lösningar till uppgiften:</p>
<select id='solution-lang' onchange='createOptionForSolution(this)'>
<option id='solution-java' value='java'>java</option>
<option id='solution-md' value='md'>psedo kod</option>
<option id='solution-cs' value='cs'>cs</option>
<option id='solution-js' value='js'>js</option>
</select>
<select id='solution-by'></select>
<button id='load-solution' onclick='loadSolution()'>Load</button>
</div>
<div id='information'>
<div><strong>Av:</strong> <div id='by'>dig</div></div>
<div><strong>Kategori:</strong><div id='category'>ingen</div></div>
<div id='solution-info' style='display:none'><strong>Lösning av:</strong> <div id='solution-by-alias'>dig</div></div>
</div>

<div id='exercise-container'>
<h2 id='exercise-title'> Uppgift </h2>
<p id='exercise'>Vad är detta?
</p>
</div>

<div id='editor-container'>
<h3 id='editor-title'> Editor </h3> 
<textarea id='editor'></textarea>
<div id='buttons'>
<button id='run' onclick='runJS()'>Kör</button><button id='testing' onclick='testAll()'>Testa</button>
</div>
</div>
   
<div id='selection-container' >
<div id='selection'>
<button id='add-Dataset' onclick='addDataset()'>+</button>
<button id='selection-0' onclick='changeDataset(0);'>0</button>
</div>
<div id='input-container'>
<h3 id='input-title'> Input </h3>  
<textarea id='in' class='io-test' ></textarea>
<h3 id='output-title'>Output</h3>
<textarea id='out' class='io-test' ></textarea>
<h3 id='test-title'>Test</h3>
<textarea id='test' class='io-test' ></textarea>
</div>
</div>

<div id='alias-submit'>
 Alias: <input type='text' name='alias'><br />
 <button id='submit'>Submit</button>
</div>

</div>
<sup id='footnote'> © 2015-2017 Magnus Kronnäs <br /> 
Det finns $number_of_exercises uppgifter, $number_of_solutions_uniq av dem har lösningar. <br />
Totalt finns det $number_of_solutions lösningar.
</sup>
</body>
</html>
" > index.html

printf "\n$Green$Bold Very easy: $veryEasy Easy: $easy Normal: $normal Hard: $hard Very Hard $veryHard Extreme: $extreme$Rst\n\n"
