//by malvedin
import java.util.Scanner;

public class Ex2 {

public static void main(String[] args) {
Scanner in = new Scanner(System.in); //setup
System.out.println("Type text"); //take inputs
String text = in.next();
System.out.println("Type key");
int key = in.nextInt();
char[] textChar = text.toCharArray(); //converts the string to make it easier to work with
for(int i = 0; i < textChar.length; i++) { //replaces each character with the encrypted character
int encChar = encrypt(textChar[i], key);
textChar[i] = (char) encChar;
}
System.out.println(textChar);
}
public static int encrypt(int input, int key) {
int newChar = input - 97;
if(newChar == 132) {newChar -= 106;}
if(newChar == 131) {newChar -= 104;}
if(newChar == 149) {newChar -= 121;}
newChar += key;
newChar = newChar % 29;
if(newChar == 26) {newChar += 106;}
if(newChar == 27) {newChar += 104;}
if(newChar == 28) {newChar += 121;}

return newChar + 97;
}

}
