# Primtal

Gör ett program som beräknar de 100 första primtalen. Det är inget krav på att din funktion ska vara effektiv.

**Tips:** Gör först en funktion som undersöker om ett tal är ett primtal eller inte. Detta kan du göra enkelt genom att testa om något tal som är mindre än talet är delbart eller inte.

by: krm 
