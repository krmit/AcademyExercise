#!/bin/sh
set -e;

TC='\033['
Rst="${TC}0m"     

Bold="${TC}1m"    
Undr="${TC}4m"    

Black="${TC}30m"; 
Red="${TC}31m"; 
Green="${TC}32m"; 
Yellow="${TC}33m";
Blue="${TC}34m";  
Purple="${TC}35m";
Cyan="${TC}36m";
White="${TC}37m";

printf "\n$Bold$Green Installing all\n";
printf "$Bold$Green Installing npm packages $Rst\n";
npm install;

printf "$Bold$Red You should install bc! $Rst\n";


printf "$Bold$Green Setting version to 1 $Rst\n";
echo "1" > ./version

printf "$Bold$Green Publising for first time $Rst\n";
./playground/publish.sh

printf "$Bold$Green Finish! $Rst \n\n";
