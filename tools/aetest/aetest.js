#!/usr/bin/nodejs

var colors = require("colors");
var shell = require("shelljs/global");
var prompt = require("prompt-sync")();

var argv = require("yargs")
.usage("Usage: aetest.js [options] [program]")

.alias("v", "verbose")
.boolean("verbose")
.describe("verbose", "Print more information about what happens.")

.alias("h", "help")
.help("h")

.argv;


