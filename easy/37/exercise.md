# En lista med några fina löv

Bakgrund
Du har en samling av vackra löv som du behöver hålla ordning på.

Definiera en lista som innehåller följande ord i den ordning som de räknas upp här:
Lönn, Björk, Ek, Bok, Asp, Alm, Pil och Rönn.

Låt en användare skriva in ett heltal och plocka ut motsvarande träd i listan. Observera att om användaren skriver en “2” så ska du plocka ut det andra trädet i listan.

by: krm
