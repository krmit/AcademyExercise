# Vad är sträckan?

Gör en funktion som motsvaras formeln 's=vt' och som gör att du kan bestämma sträckan. Du ska själv välja lämpliga parametrar och retur värden. Gör sedan ett program som låter en användare skriver in värden på 'v' och 't' för att sedan få svaret. 
 
by: krm
