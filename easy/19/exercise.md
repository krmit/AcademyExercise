# Definera en funktion som ger oss arean av en kub

Gör en funktion som tar en inparameter a som motsvara kubenssidor och returnerar sedan arean för en kub. Låt sedan en användare skriva in värden för att testa denna.

Du kan beräkna arena med följande formel: 6*a*a

by: krm
