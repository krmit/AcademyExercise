# Absolutbelopp av en funktion

Gör en funktion som beräknar absolutbeloppet av ett decimaltal. Du kan själv välja passande datatyp, men du får inte använda dig av en funktion från math bibliotek.

En användare ska kunna testa din funktion.

by: krm, sweecrow
