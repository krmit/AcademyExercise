# Funktionen a och b samt en 4:a

Kopiera koden nedan. Anropa funktionen a med värdet 4. Anropa sedan funktion b med detta värde. Skriv ut resultatet.

function a(x) {
     return x*x;
}

function b(x) {
     return x**x;
}

by: krm
