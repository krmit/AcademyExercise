# Tre parametrar till en funktion

Kopiera koden nedan. Låt en användare skriva in tre tal och anropa sedan funktionen a med dessa värden och skriv ut resultatet.

function a(x,y,z) {
     return (x+y)**z;
}


by: krm
