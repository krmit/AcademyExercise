# Addera 3 många gånger.

Om du addera många gånger så  blir det multiplikation. 

Definiera en funktion som heter “add3” och tar en parameter. Den ska sedan returnera parameter adderat med 3. 

Du ska sedan låta funktionen anropa sig själv tre gånger utifrån ett värde. Se exemplet nedan där funktionen anropar sig själv två gånger och börjat med värdet 3:

add3(add3(3));

En användare ska ange vilket värde anropen ska börja med och du ska också skriva ut resultat.


by: krm
