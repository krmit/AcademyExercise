# Hur många timmar är det kvar?

Fråga en användare tre frågor:
  * Hur många år är det kvar?
  * Hur många dagar är det kvar?
  * Hur många timmar är det kvar? 

Skriv sedan ut hur många timmar det är kvar.

**Tips:** Ett är är 8766 timmar(enligt Julianska kalendern).

by: krm
