# Ohms lag gäller alltid

Gör en funktion som tar två parametrar I och R. Funktionen ska sedan med hjälp av Ohms lag beräkna U. Det vill säga multiplicera I och R och returnera resultatet.

Låt en användare ange värdet på I och R samt returnera resultatet.

by: krm
