# Slå en T6.

Vi ska spela ett sälskapsspel men har ingen tärningar. Vi behöver en sex sidig tärning, så kallad T6. Gör ett program som slumpar fram ett tal mellan 1 till 6. 

**Obs:** Test kommer inte vara relevanta. 
**Tips:** Om vi vill ha ett slumptal mellan tal A och ett större tal B. Så använder vi en "random" funktion för att få en slumpad decimaltal mellan 0 och 1. Sedan så tar vi först antal steg från A till B, dvs A-B+1, och multiplicerar det med talet. Vi avrundar produkten nedåt för att sedan addera A. Till exempel om vi vill slumpa ett heltal som mellan 3 och 9 så slumpar vi ett tal och multiplicerar med 6, avrundar nedåt och adderar sedan 3. Vi får då ett av talen 3,4,5,6,7,8,9.
  
**js:** Math.floor(Math.random()*antal_steg)+minsta_tal;
**java:** Math.floor(Math.random()*antal_steg)+minsta_tal;
 
by: krm 
