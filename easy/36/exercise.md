# Ready or not!

Ofta vill ett program veta om det ska utföra en åtgärd eller inte.

Skriv ett program som skriv “Ready!” om en användare antingen skriver “y”, “yes” eller “Yes”. Annars ska programmet skriva ut “Not!”.

by: krm
