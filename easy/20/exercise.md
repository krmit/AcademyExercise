# Räkna ifrån 1024 till 1337

Gör ett program som skriver ut alla tal ifrån 1024 till 1337. Varje tal ska stå på en ny rad. Ditt program får vara max 13 rader långt.

variant: VE2
extension: N1
by: krm
