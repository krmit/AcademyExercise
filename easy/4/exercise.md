# Boksatav till tal

Låt oss numrera våra tal så att “a” är 1 och “b” är 2 osv. Gör ett program som tar en bokstav och returnerar talet för denna bokstav. Du behöver bara göra för talen a-z. 

tips: Du kan använda följande hjälpfunktion.
js: *.charCodeAt(0)*    
 
by: krm
