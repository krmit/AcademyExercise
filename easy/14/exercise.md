# Fråga en användare om namn och ålder.

Låt programmet fråga en användare "Hur gammal är du?" och "Vad heter du?".

Skrev sedan ut "Du heter X och är Y år gammal." Där X är namnet som användaren skrivit in och Y är hur gammal användaren är.
  
by: krm
