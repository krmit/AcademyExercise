# Academy Exercise

This program should have been abandoned several years ago. But I think 
it's easier to just continue.

# Installation

Get the git repo.

```
git clone git@gitlab.com:krmit/AcademyExercise.git
```

Install the **mo** script with help of shell script.

```
./tools/install.sh 
```

# Use

To publish exercises have use script in playgorund.

```
./playground/publish.sh 
```

# Terminology

## Level of difficulty
