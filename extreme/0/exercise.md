# Vigenère-chiffer

Vi ska ny göra ett lite svårare variant av Caesarchiffer som kallas Vigenère-chiffer. Vi utgår ifrån ett nyckelord t.ex. “abcd”. Första bokstaven i “abcd” är “a” som vi låter representeras av talet 0. Nästa bokstav är b, vilket är en 1. Vi får alltså talen “0,1,2,4”. Sedan tar vi ett ord som “katt” och kodar det med nyckeln “abcd”. Förata bokstaven “k” flyttar vi fram 0 steg, efter som vi utgår ifrån vår nyckel. Nästa bokstav flyttar vi fram 1 steg så den blir b. På samma vis får vi att “t” blir “v” och “t” blir “w”. Gör ett program som låter användaren både kryptera och dekryptera, användare väljer vilket genom först skriva "c" för kryptera och "d" för dekryptera. För enkelhetens skull så låt nyckeln och ordet som ska krypteras vara lika stora. 

previus: VH2
by: krm
