#  Zipfs lag.

Om texten vi räknar ord på är tillräckligt stor så borde vi se ett intressant samband som kallas [Zipfs lag|https://sv.wikipedia.org/wiki/Zipfs_lag].

Det säger att om det vanligaste ordet förekommer N gånger så kommer nästa vanligaste ord att förekomma N/2. Sambandet forsätter så det tredje vanligaste ordet är N/3, fjärde vanligaste N/4 osv. Gör ett program som demonstrerar detta och upptäcker detta samband i en text om det exsisterar!

Kommentar: En rolig genomgång av detta och likande samaband är [denna|https://www.youtube.com/watch?v=fCn8zs912OE] youtube film. 

