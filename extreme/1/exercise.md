# Ackerman funktionen

 En annan rolig funktion som finns är den så kallade Ackerman funktionen, här presenterad i en förenklad form. Ackerman växer så snabbt att detta har teoretiska implikationer för datavetenskapen. Nedan följer en förenklat definition, men observera att villkoren måste testas och utföras i den ordning som de står här:

Ack(0,x)=x+1 
Ack(n,0)=Ack(n-1,1) 
Ack(n,x)=Ack(n-1,Ack(n, x-1))

Implementera denna funktion.

**Tips:** Denna uppgift är egentligen inte så svår. Bara försök göra om Ack till en funktion som anropar sig själv och det kommer nog gå bra. 

by: krm, Crash
category: recursion
