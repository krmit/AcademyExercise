//by peronet

import java.util.Scanner;

public class Ackerman {

	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int tal1 = in.nextInt();
		int tal2 = in.nextInt();
		
		System.out.println(function(tal1,tal2));
				
		in.close();
	}

	private static int function(int n, int x) {
	
		if(n==0)
		{
		x += 1;	
		return x;
		}
		
		else if(x==0)
		{
		x += 1;
		return function(n-1, x);
		}
		
		else
		{
		return function(n-1,function(n,x-1));
		}
	}

}
