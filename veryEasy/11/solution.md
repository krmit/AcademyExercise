**by:** krm
# Lösning med for-loopar

Definera en heltals variabel "factor" som ska vara **1**.

Börja göra en *loop*.
Sätt en variabel **i** till **1**.
Undersök om **i** är mindre än **11**.
Öka **i** med **1** för varje loop.
I vaje loop gör följande:
    Sätt faktor till faktor multiplicerat med **i**.
