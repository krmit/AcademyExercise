# Deklarera HTS! 

Vi ska ha en lista över aktiva teknikinriktningar på HTS. Men eftersom detta varierar så mycket behöver vi ha en dynamisk lista som innehåller namnen på inriktningarna. För närvarande är inriktningarna: IT, design, bygg, produktion och trollvetenskap.

category: list
by: krm
