# Räkna bokstäver

Gör en funktion som tar en “string” som inparameter och returnerar hur många tecken strängen innehåller. 

tips: Om du har sparat texten *“hej”* i en variabel *s*. Så kan du få längens genom att använda funktionen:
js: *s.length()*
  
by: krm
