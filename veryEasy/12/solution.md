**by:** krm
# Lösning med for-loopar

Definera en heltals variabel **big_number** som ska vara **2310**. 
Definera en decimaltals variabel **smaller_number** som ska vara **big_number** delat med **7**.

Gör en **if**-sats som ser om **smaller_number** är större än 330.
Om så:
        Skriv "Sant".
Annars:
        Skriv "Falskt"


