# Räkna ifrån 1 till 100

Gör ett program som skriver ut alla tal ifrån 1 till 100. Varje tal ska stå på en ny rad. Ditt program får vara max 99 rader långt.
  
extension: N1
by: krm
