
Definera en funktion som heter **halvera**.
Den ska ha en inparameter:
**tal** som är ett heltal.
I funktion gör följande:
    Definera en heltalsvariabel resultat.
    Sätt denna variabel till **tal** delat med **2**.
    Returnera resultat.

