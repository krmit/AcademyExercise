# Beräkna arean av en cirkel.

Beräkna arean av en cirkel med radien 9 och skriv ut den.

tips: Du bör använda konstanten *pi* ifrån biblioteket:
js: *math*
  
by: krm
